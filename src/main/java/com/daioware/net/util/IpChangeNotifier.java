package com.daioware.net.util;

import java.io.File;
import java.io.IOException;

import com.daioware.file.MyRandomAccessFile;

public abstract class IpChangeNotifier implements Runnable{
	
	private File currentIpFile;
	private String machineName;
	private String ip;
	
	public IpChangeNotifier(File currentIpFile, String machineName, String ip) {
		this.currentIpFile = currentIpFile;
		this.machineName = machineName;
		this.ip = ip;
	}

	public File getCurrentIpFile() {
		return currentIpFile;
	}
	public void setCurrentIpFile(File currentIpFile) {
		this.currentIpFile = currentIpFile;
	}
	public String getMachineName() {
		return machineName;
	}

	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}

	public String getIp() {
		return ip;
	}

	protected void setIp(String ip) {
		this.ip = ip;
	}

	protected void writeIpToFile() {
		try {
			MyRandomAccessFile r=new MyRandomAccessFile(getCurrentIpFile(),"rw");
			r.setLength(0);
			r.writeCharacters(getIp());
			r.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public abstract void notify(String machineName,String oldIp,String newIp);
	
	public void run() {
		String newIp;
		String oldIp=getIp();
		try {
			newIp = InternetService.getPublicIpAddress();
			if(!newIp.equals(oldIp)) {
				notify(getMachineName(), oldIp, newIp);
				setIp(newIp);
				writeIpToFile();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
}
