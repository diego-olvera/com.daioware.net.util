package com.daioware.net.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.daioware.file.FileUtil;
import com.daioware.file.MyRandomAccessFile;
import com.daioware.mail.client.MailBody;
import com.daioware.mail.client.MailMessenger;
import com.daioware.mail.client.MailReceiver;
import com.daioware.mail.client.MailUser;
import com.daioware.mail.client.SendMailException;
import com.daioware.templates.Template;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public class IpChangeNotifierByMail extends IpChangeNotifier{
	
	private MailMessenger mailMessenger;
	
	public static IpChangeNotifierByMail getIpNotifier(File file) throws JsonSyntaxException, IOException{
		JsonObject jelement = new JsonParser().parse(FileUtil.getContents(file)).getAsJsonObject();
		JsonObject sender = jelement.get("sender").getAsJsonObject();
		String machineName=jelement.get("machineName").getAsString();
		String currentIpFile=jelement.get("currentIpFile").getAsString();
		MailUser user=new MailUser(sender.get("username").getAsString(), sender.get("password").getAsString());
		List<MailReceiver> receivers=new ArrayList<>();
		for(JsonElement el:jelement.get("receivers").getAsJsonArray()) {
			jelement=el.getAsJsonObject();
			receivers.add(new MailReceiver(jelement.get("address").getAsString(),jelement.get("type").getAsString()));
		}
		return new IpChangeNotifierByMail(user, receivers,machineName,FileUtil.getCompatibleFile(currentIpFile));
	}
	public IpChangeNotifierByMail(MailUser mailUser, List<MailReceiver> receivers,String machineName,
			File currentIpFile) throws IOException {
		super(currentIpFile, machineName, null);
		if(!currentIpFile.exists()) {
			currentIpFile.createNewFile();
		}
		setMailMessenger(new MailMessenger(null,null, receivers));
		mailMessenger.setDefaultUser(mailUser);
		setMachineName(machineName);
		setCurrentIpFile(currentIpFile);
		setIp(FileUtil.getContents(currentIpFile));
	}

	public MailMessenger getMailMessenger() {
		return mailMessenger;
	}

	public void setMailMessenger(MailMessenger mailMessenger) {
		this.mailMessenger = mailMessenger;
	}

	protected MailBody getBody(String oldIp,String newIp) {
		Template t=new Template("<html>The ip <strong>$oldIp</strong> has changed to "
				+ "<strong>$newIp</strong></html>");
		t.put("oldIp",oldIp);
		t.put("newIp",newIp);
		return new MailBody(t.getTextProcessed(),MailBody.HTML_TEXT);
	}
	protected void writeIpToFile() {
		try {
			MyRandomAccessFile r=new MyRandomAccessFile(getCurrentIpFile(),"rw");
			r.setLength(0);
			r.writeCharacters(getIp());
			r.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void notify(String machineName, String oldIp, String newIp) {
		MailMessenger messenger=getMailMessenger();
		messenger.setSubject(String.format("%s has changed IP",machineName));
		messenger.setBody(getBody(oldIp,newIp));
		try {
			messenger.send();
		} catch (SendMailException e) {
			e.printStackTrace();
		}
		
	}	
}
